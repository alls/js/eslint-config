import ts from 'typescript-eslint'
import common from './index.js'

export default ts.config(
	...common,
	{
		languageOptions: {
			parserOptions: {
				projectService: true,
				tsconfigRootDir: import.meta.dirname,
			},
		},
	},
)
