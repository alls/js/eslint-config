import stylistic from '@stylistic/eslint-plugin'
import ts from 'typescript-eslint'

export default ts.config(
	...ts.configs.strictTypeChecked,
	...ts.configs.stylisticTypeChecked,
	{
		rules: {
			'@typescript-eslint/no-namespace': 'off',
			'@typescript-eslint/no-explicit-any': 'off',
			'@typescript-eslint/no-dynamic-delete': 'off',
			'@typescript-eslint/only-throw-error': 'off',
			'@typescript-eslint/consistent-type-definitions': 'off',
			'@typescript-eslint/restrict-template-expressions': ['warn', {
				allowBoolean: true,
				allowNullish: true,
				allowNumber: true,
				allowRegExp: true,
			}],
			'@typescript-eslint/no-unnecessary-condition': ['warn', {
				checkTypePredicates: false,
			}],
			'@typescript-eslint/prefer-nullish-coalescing': ['warn', {
				ignorePrimitives: { string: true, boolean: true },
			}],
			'@typescript-eslint/no-empty-function': 'off',
			'@typescript-eslint/no-non-null-assertion': 'off',
			'@typescript-eslint/no-unused-vars': ['warn', {
				args: 'all',
				ignoreRestSiblings: true,
				argsIgnorePattern: '^_',
			}],
		},
	},
	stylistic.configs['recommended-flat'],
	{
		rules: {
			'@stylistic/no-tabs': 'off',
			'@stylistic/max-statements-per-line': 'off',
			'@stylistic/indent': ['warn', 'tab', { ArrayExpression: 1 }],
			'@stylistic/indent-binary-ops': ['warn', 'tab'],
			'@stylistic/quotes': ['warn', 'single', { avoidEscape: true }],
			'@stylistic/semi': ['warn', 'never'],
			'@stylistic/max-len': ['warn', {
				tabWidth: 2,
				ignoreUrls: true,
			}],
			'@stylistic/arrow-parens': ['warn', 'as-needed'],
			'@stylistic/operator-linebreak': ['warn', 'after',
				{ overrides: { '?': 'before', ':': 'before' } }],
		},
	},
	{
		rules: {
			'array-bracket-newline': ['warn', 'consistent'],
			'comma-dangle': ['warn', 'always-multiline'],
		},
		ignores: ['**/build',	'**/.*', '**/dist', '**/node_modules',
			'**/*-report', '**/*-results', '**/*.css'],
	},
)
